import os
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup
from ordered_set import OrderedSet
from tqdm import tqdm


def main():
    if not os.path.exists('pages'):
        os.makedirs('pages')

    links = OrderedSet(['https://ru.wikipedia.org/wiki/Заглавная_страница'])
    cnt = 1
    i = -1

    with tqdm(total=100) as pbar:
        while links:
            if cnt > 100:
                break

            i += 1
            try:
                link = links[i]
            except IndexError:
                print('Закончились ссылки')
                break

            try:
                response = requests.get(link)
                if response.status_code != 200:
                    continue
                soup = BeautifulSoup(response.content, 'html.parser')
            except Exception as e:
                pass

            for a_soup in soup.find_all('a'):
                page_link = a_soup.get('href')
                if (
                    page_link and
                    len(page_link) > 0 and
                    page_link[0] != '#' and
                    page_link != '/'
                ):
                    links.append(urljoin(link, page_link))

            text = soup.get_text(separator=' ', strip=True)
            if len(text.split()) < 1000:
                continue

            with open(f'pages/{cnt}.txt', 'w') as f:
                f.write(text)

            with open(f'pages/index.txt', 'a') as f:
                f.write(f'{cnt}: {link}\n')

            cnt += 1
            pbar.update(1)


if __name__ == '__main__':
    main()
