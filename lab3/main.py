import re

from tqdm import tqdm


def create_index():
    index = {}

    for i in tqdm(range(1, 101)):
        with open(f'../lab2/tokens/{i}.txt') as f:
            tokens = set(f.read().split('\n'))

        for token in tokens:
            if token not in index:
                index[token] = []
            index[token].append(i)

    with open('indexes_list.txt', 'w') as f:
        for key in sorted(index):
            f.write(f'{key}: ' + ' '.join(str(x) for x in index[key]) + '\n')


def search(search_line: str):
    search_keys = {x: None for x in re.sub(r'[&|!]', ' ', search_line).split()}

    with open('indexes_list.txt', 'r') as f:
        lines = f.read().split('\n')

    for line in lines:
        if line == '':
            continue
        key, pages_str = line.split(': ')
        if key in search_keys:
            search_keys[key] = set(int(x) for x in pages_str.split())

    all_pages = set(range(1, 101))

    or_pages = set()
    or_lst = search_line.split(' | ')
    for or_el in or_lst:
        and_pages = all_pages
        and_lst = or_el.split(' & ')
        for and_el in and_lst:
            pages = (
                all_pages.difference(search_keys[and_el[1:]])
                if and_el[0] == '!'
                else search_keys[and_el]
            )
            and_pages = and_pages & pages
        or_pages = or_pages | and_pages

    return or_pages


def main():
    create_index()

    word1, word2, word3 = 'красивый', 'область', 'дверь'
    search_lines = [
        f'{word1} & {word2} | {word3}',  # {2, 3, 4, 5}
        f'{word1} | {word2} | {word3}',  # {1, 2, 3, 4, 5}
        f'{word1} & {word2} & {word3}',  # {3}
        f'{word1} & !{word2} | !{word3}',  # {1, 2, 3} & {1, 5, 6, 7, 8} | {1, 2, 6, 7, 8} = {1, 2, 6, 7, 8}
        f'{word1} | !{word2} | !{word3}'  # {1, 2, 3} | {1, 5, 6, 7, 8} | {1, 2, 6, 7, 8} = {1, 2, 3, 5, 6, 7, 8}
    ]
    for search_line in search_lines:
        print(' '.join(str(x) for x in search(search_line)))


if __name__ == '__main__':
    main()
