import csv


def main():
    all_tokens = set()
    docs = []
    for i in range(1, 101):
        doc_tokens = {}
        # with open(f'lab4/tf/{i}.csv', 'r') as old_tf_csv:
        with open(f'lab4/tf-idf/{i}.csv', 'r') as old_tf_csv:
            old_tf_csv_reader = csv.reader(old_tf_csv, delimiter=';')
            for line in old_tf_csv_reader:
                doc_tokens[line[0]] = float(line[1])
                all_tokens.add(line[0])
        docs.append(doc_tokens)

    # with open(f'lab4/tf.csv', 'w') as new_tf_csv:
    with open(f'lab4/tf-idf.csv', 'w') as new_tf_csv:
        new_tf_csv_writer = csv.writer(new_tf_csv, delimiter=';')
        new_tf_csv_writer.writerow(['token', *list(range(1, 101))])
        for token in sorted(all_tokens):
            line_lst = [token]
            for i in range(0, 100):
                line_lst.append(docs[i].get(token, 0.0))
            new_tf_csv_writer.writerow(line_lst)


if __name__ == '__main__':
    main()
