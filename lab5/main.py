import csv
import math

import pymorphy2


def search(search_line: str):
    morph = pymorphy2.MorphAnalyzer()
    search_tokens = [morph.parse(word)[0].normal_form for word in search_line.split()]

    # Get idf
    idf = {}
    with open('../lab4/idf.csv', 'r') as idf_csv:
        idf_csv_reader = csv.reader(idf_csv, delimiter=';')
        for line in idf_csv_reader:
            if line[0] in search_tokens:
                idf[line[0]] = float(line[1])

    # Get TF-IDF for search
    tf_idf_for_search = {}
    for search_token in set(search_tokens):
        current_tf = round(search_tokens.count(search_token) / len(search_tokens), 5)
        current_idf = idf.get(search_token, round(math.log10(100 / 1), 5))
        tf_idf_for_search[search_token] = current_tf * current_idf

    ratings = []
    for i in range(1, 101):

        # Get TF-IDF for doc
        tf_idf_for_doc = {}
        with open(f'../lab4/tf-idf/{i}.csv', 'r') as tf_idf_csv:
            tf_idf_csv_reader = csv.reader(tf_idf_csv, delimiter=';')
            for line in tf_idf_csv_reader:
                tf_idf_for_doc[line[0]] = float(line[1])

        # Calculate cosine similarity between search and doc vectors
        all_tokens = set(tf_idf_for_doc.keys()) | set(tf_idf_for_search.keys())
        doc_vector = [tf_idf_for_doc.get(t, 0) for t in all_tokens]
        search_vector = [tf_idf_for_search.get(t, 0) for t in all_tokens]
        ratings.append((i, cosine_similarity([doc_vector], [search_vector])[0][0]))

    return sorted(ratings, key=lambda x: x[1], reverse=True)


def main():
    word1, word2, word3 = 'область', 'красивых', 'дверей'
    search_lines = [
        f'{word1}',
        f'{word1} {word2}',
        f'{word1} {word2} {word3}'
    ]

    for search_line in search_lines:
        print('\n'.join([f'{y[0]}: {y[1]}' for y in search(search_line)]))


if __name__ == '__main__':
    main()
