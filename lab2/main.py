import os
import re

import pymorphy2
from tqdm import tqdm


def main():
    if not os.path.exists('tokens'):
        os.makedirs('tokens')

    morph = pymorphy2.MorphAnalyzer()
    stop_tags = ['NUMB', 'PREP', 'CONJ', 'PRCL', 'NPRO', 'LATN', 'UNKN']

    for i in tqdm(range(1, 101)):
        with open(f'../lab1/pages/{i}.txt') as f:
            text = f.read()

        words = list()
        for word in re.sub(r'[^\w\s-]', ' ', text).split():
            word_info = morph.parse(word)[0]
            if not any([stop_tag in word_info.tag for stop_tag in stop_tags]):
                word_normalized = word_info.normal_form
                if re.fullmatch(r'[(а-я)-]+', word_normalized) and word_normalized[0] != '-':
                    words.append(word_normalized)

        with open(f'tokens/{i}.txt', 'w') as f:
            f.write('\n'.join(words))


if __name__ == '__main__':
    main()
