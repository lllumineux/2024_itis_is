import csv
import math
import os
from collections import defaultdict

from tqdm import tqdm


def main():
    if not os.path.exists('tf'):
        os.makedirs('tf')
    if not os.path.exists('tf-idf'):
        os.makedirs('tf-idf')

    # Calculate IDF
    idf = {}
    with open('../lab3/indexes_list.txt', 'r') as f:
        lines = f.read().split('\n')
    with open('idf.csv', 'w') as idf_csv:
        idf_csv_writer = csv.writer(idf_csv, delimiter=';')
        for line in lines:
            if line == '':
                continue
            key, pages_str = line.split(': ')
            idf[key] = round(math.log10(100 / len(pages_str.split())), 5)
            idf_csv_writer.writerow([key, idf[key]])

    for i in tqdm(range(1, 101)):
        with open(f'../lab2/tokens/{i}.txt') as f:
            tokens = f.read().split('\n')

        # Calculate TF & TF-IDF
        per_doc = defaultdict(int)
        for token in tokens:
            if token == '':
                continue
            per_doc[token] += 1
        with open(f'tf/{i}.csv', 'w') as tf_csv, open(f'tf-idf/{i}.csv', 'w') as tf_idf_csv:
            tf_csv_writer = csv.writer(tf_csv, delimiter=';')
            tf_idf_csv_writer = csv.writer(tf_idf_csv, delimiter=';')
            for token in sorted(set(tokens)):
                token_tf = round(per_doc[token] / len(tokens), 5)
                tf_csv_writer.writerow([token, token_tf])
                token_tf_idf = round(token_tf * idf[token], 5)
                tf_idf_csv_writer.writerow([token, token_tf_idf])


if __name__ == '__main__':
    main()
